import { browser } from "protractor";
import  Util  from "./common/helpers/util.helper";

exports.config = {
    directConnect: true,

    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        'browserName': 'chrome',
        chromeOptions: {
            args: ['--disable-gpu']
        }
    },

    // Framework to use. Jasmine is recommended.
    framework: 'jasmine2',

    // Options to be passed to Jasmine.
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 90000,
        isVerbose: true
    },
    specs: ['./test-suites/*.js'],
    baseUrl: "https://mobidram.mts.am/en/home",

    onPrepare() {
        Util.addReporert();
        browser.manage().window().maximize();
    }
}