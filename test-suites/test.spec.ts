import {browser} from 'protractor';

describe('Page Tests: ', () => {
    beforeAll(() => {
        // This is false because we are testing non-angular app.
        browser.waitForAngularEnabled(false);
         browser.get(browser.baseUrl);
    });

    it('Verify page title', function () {
        browser.getTitle().then(function (title) {
            expect(title).toEqual('Home');
        })
    });
});