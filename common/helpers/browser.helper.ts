/**
 * Created by Aleksandr Hakobyan 
 */

import { browser, ExpectedConditions, promise } from 'protractor';
import { TimeOut } from '../enums/constant.enum';

/**
 * This class provides browser helper functions
 */
export class Browser {

    /**
     * This method switch to the tab with the given index
     * @param index
     */
    public static async switchTab(index: number): promise.Promise<void> {
        let handles = await browser.getAllWindowHandles();
        await browser.switchTo().window(handles[index]);
    }

    /**
     * This method gets current url
     */
    public static async getCurrentUrl(): promise.Promise<string> {
        return await browser.getCurrentUrl();
    }

    /**
     * This method close browser current tab
     */
    public static async close(): promise.Promise<void> {
        await browser.close();
    }

    /**
     * This method gets page title
     */
    public static async getPageTitle(): promise.Promise<string> {
        return await browser.getTitle();
    }

    /**
    * This method scrolls page Up/Down to the given coordinates
    */
    public static async scroll(x: number, y: number): Promise<void> {
        await browser.executeScript(`window.scrollTo(${x},${y})`);
    }

    /**
     * This method waits for url to contain given suburl param
     */
    public static async waitForUrlContains(url: string): Promise<void> {
        await browser.wait(ExpectedConditions.urlContains(url), TimeOut.DEFAULT, `wait for \'${url}\' page`);
    }

    /**
     * This method prforms refresh/back/forward actions for browser
     * @param action browser action (can have the following refresh/back/forward)
     */
    public static async action(action: string): Promise<void> {
        await browser.navigate()[action]()
    }
}
