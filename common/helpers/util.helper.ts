export default class Util {
    static addReporert() {
        // Require protractor-beautiful-reporter to generate reports.
        let HtmlReporter = require('protractor-beautiful-reporter')
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'test-results',
            preserveDirectory: true, // Preserve base directory
            screenshotsSubfolder: 'screenshots',
            jsonsSubfolder: 'jsons', // JSONs Subfolder
            takeScreenShotsForSkippedSpecs: true, // Screenshots for skipped test cases
            takeScreenShotsOnlyForFailedSpecs: false, // Screenshots only for failed test cases
            docTitle: 'Test Automation Execution Report', // Add title for the html report
            docName: 'TestResult.html', // Change html report file name
            getBrowserLogs: false, // Store Browser logs
            gatherBrowserLogs: false
        }).getJasmine2Reporter());
    }
}