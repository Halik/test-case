import {ElementFinder, ElementArrayFinder} from 'protractor';

export interface ElementInterface  {
    name: string;
    selector?: string;
    isVisible?: boolean;
    postfix?: boolean;
    text?: string;
    isSelected?: boolean;
    placeholder?: string;
    group?: string | string[];
    index?: number;
    url?: string;
}
