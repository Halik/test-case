
## To Get Started

### Pre-requisites
* Download and install Chrome or Firefox browser.
* Download and install Node.js:
* Optional - Download and install any Text Editor like Visual Code/WebStorm

### Setup Scripts 
* Clone the repository into a folder
* Install Protractor: `npm install -g protractor`
* Go to Project root directory and install Dependency: `npm install`
* All the dependencies from package.json  would be installed in node_modules folder.
* Update necessary binaries of webdriver-manager: `webdriver-manager update` 

### How to Run Test
* Run complete Test Suite: `npm test`

### How to Update local npm packages
* Go to Project root directory and run command: `npm update`